<?php

namespace App\Services;


class Proxy
{
    private $proxy_login    = 'yzendev';
    private $proxy_password = 'R2m2EjR';
    private $proxy_port     = '65233';
    private $proxy_array    = [
        '185.128.212.168',
        '46.8.211.105',
        '89.191.235.56',
        '109.120.174.3',
    ];

    private $index = 0;

    public function getProxy()
    {
        $ip = $this->proxy_array[$this->index];
        if (++$this->index > 3) $this->index = 0;
        return 'http://' . $this->proxy_login . ':' . $this->proxy_password . '@' . $ip . ':' . $this->proxy_port;
    }
}