<?php

namespace App\Services\Parsing;


use App\Models\Sites\DomClick;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class ParserDomClick implements Parser
{

    /**
     * Запуск парсера
     *
     * @param  string|null $category
     *
     * @return void
     */
    static function start($category = null)
    {
        if (empty($category)) {
            self::parsingAllCategories();
        } else {
            self::parsingCategory($category);
        }
    }

    /**
     * Парсинг заданной категории
     *
     * @param  string|null $category
     *
     * @return void
     */
    static function parsingCategory($category)
    {
        switch ($category) {
            case 'all':
                self::parsing(DomClick::CATEGORY_ALL);
                break;
            case 'flat':
                self::parsing(DomClick::CATEGORY_FLATS);
                break;
            case 'complex':
                self::parsing(DomClick::CATEGORY_FLATS);
                break;
            case 'room':
                self::parsing(DomClick::CATEGORY_ROOMS);
                break;
            case 'house':
                self::parsing(DomClick::CATEGORY_HOUSES);
                break;
            case 'land':
                self::parsing(DomClick::CATEGORY_LANDS);
                break;
            case 'townhouse':
                self::parsing(DomClick::CATEGORY_TOWNHOUSE);
                break;
            case 'house-part':
                self::parsing(DomClick::CATEGORY_HOUSE_PART);
                break;
        }
    }

    /**
     * Парсинг всех категорий
     *
     * @return void
     */
    public static function parsingAllCategories()
    {
        self::parsing(DomClick::CATEGORY_ALL, 1, 100);
    }

    /**
     * Парсинг всех категорий
     *
     * @param  string              $category
     * @param  string|integer|null $startPage
     * @param  string|integer|null $endPage
     *
     * @return void
     */
    public static function parsing($category = DomClick::CATEGORY_ALL, $startPage = 1, $endPage = 100)
    {
        $baseURL = DomClick::URL_LIST . '&' . $category;

        $offset = 0;
        for ($numberPage = $startPage; $numberPage <= $endPage; $numberPage++) {
            try {
                $url          = $baseURL . '&offset=' . $offset . '&limit=' . DomClick::LIMIT;
                $guzzleClient = new Client();
                $resultQuery  = $guzzleClient->request('GET', $url, []);
                $resultQuery  = \json_decode($resultQuery->getBody()->getContents());
                $objects      = $resultQuery->result->items;
                dd($objects);
                var_dump('СТРАНИЦА ' . $numberPage . '; Объявлений: ' . count($objects));
                foreach ($objects as $object) {
                    try {
                        $result = self::createObject($object);
                        if ($result) sleep(13);
                    } catch (\Throwable $exception) {
                        var_dump('Ошибка создания');
                        var_dump($object);
                        var_dump($exception);
                    }
                }
                $offset += DomClick::LIMIT;
            } catch (GuzzleException $error) {
                var_dump($error);
                //Запись в лог
            } catch (\Exception $e) {
                var_dump($e);
            }
        }
    }

    private static function createObject($object)
    {
        if ($object->value->category->id !== DomClick::CATEGORY_FLATS) {
            var_dump('Не обрабатываемая категория: ' . $object->offer_type);
            return false;
        }

        var_dump($object->id);
        if (BaseObject::where('source', '=', 'https://www.avito.ru' . $object->value->uri_mweb)->first()) {
            var_dump('Повторное объявление');
            return false;
        }
        $url_for_object     = Avito::URL_OBJECT . $object->value->id . '?' . Avito::FIELD_API_KEY . '=' . Avito::API_KEY . '&' . Avito::PARAMETRS_FOR_OBJECT;
        $guzzleClientObject = new Client();
        $resultQuery        = $guzzleClientObject->request('GET', $url_for_object, []);
        $objectInfo         = \json_decode($resultQuery->getBody()->getContents());


        var_dump('Разбор объявления');
        $builder = AvitoBuilderObjectFactory::getBuilder($objectInfo);
        if (empty($builder)) {
            var_dump('Не удалось определить билдер для ' . $object->value->uri_mweb);
            return false;
        }
        $object = new BaseObjectFactory($builder);
        var_dump('Объявление добавлено');
        return true;
    }
}