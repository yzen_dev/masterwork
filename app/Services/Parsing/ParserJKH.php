<?php

namespace App\Services\Parsing;


use App\Models\JKH;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;

/**
 * Парсер для сайта Авито
 *
 * @package App\Services
 */
class ParserJKH
{
    /**
     * Запуск парсер
     *
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function start()
    {
        self::parsingAllCategories();
    }


    /**
     * Парсинг всех категорий
     *
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function parsingAllCategories()
    {
        self::parsing();
    }

    /**
     * Парсинг всех категорий
     *
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function parsing()
    {
        for ($i = 1; $i < 5876; $i++) {
            $cookieJar    = CookieJar::fromArray([
                'ASP.NET_SessionId' => '',
                '.ASPXAUTH'         => ''
            ], '');
            $guzzleClient = new Client(['cookies' => true]);
            $resultQuery  = $guzzleClient->request('POST', 'http:// /regop_chita/action/realityobject/get?id=' . $i, [
                'cookies' => $cookieJar
            ]);
            $result       = $resultQuery->getBody()->getContents();
            $jkh          = new JKH();
            $jkh->jkh_id  = $i;
            $jkh->result  = $result;
            $jkh->save();
            echo 'Получин дом' . $i . PHP_EOL;
        }
    }

}