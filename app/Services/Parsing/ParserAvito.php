<?php

namespace App\Services\Parsing;

use App\Factories\AvitoBuilderObjectFactory;
use App\Factories\BaseObjectFactory;
use App\Models\BaseObject;
use App\Models\Sites\Avito;
use App\Services\Proxy;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Log;

/**
 * Парсер для сайта Авито
 *
 * @package App\Services
 */
class ParserAvito implements Parser
{
    /**
     * Запуск парсера
     *
     * @param  string|null $category
     * @param  string      $city
     *
     * @return void
     */
    public static function start($city, $category = null)
    {
        if (empty($category)) {
            self::parsingAllCategories($city);
        } else {
            self::parsingCategory($city, $category);
        }
    }

    /**
     * Парсинг заданной категории
     *
     * @param  string      $city
     * @param  string|null $category
     *
     * @return void
     */
    public static function parsingCategory($city, $category)
    {
        switch ($category) {
            case 'all':
                self::parsing($city,Avito::CATEGORY_ALL);
                break;
            case 'flat':
                self::parsing($city,Avito::CATEGORY_FLATS);
                break;
            case 'room':
                self::parsing($city,Avito::CATEGORY_ROOMS);
                break;
            case 'house':
                self::parsing($city,Avito::CATEGORY_HOUSES);
                break;
            case 'land':
                self::parsing($city,Avito::CATEGORY_LANDS);
                break;
            case 'garage':
                self::parsing($city,Avito::CATEGORY_GARAGES);
                break;
            case 'commercial':
                self::parsing($city,Avito::CATEGORY_COMMERCIAL_REAL);
                break;
        }
    }

    /**
     * Парсинг всех категорий
     *
     * @return void
     */
    public static function parsingAllCategories($city)
    {
        self::parsing($city, Avito::CATEGORY_ALL, 1, 100);
    }

    /**
     * Парсинг всех категорий
     *
     * @param  string              $category
     * @param  string|integer|null $startPage
     * @param  string|integer|null $endPage
     *
     * @return void
     */
    public static function parsing($city, $category = Avito::CATEGORY_ALL, $startPage = 1, $endPage = 150)
    {
        $cities      = [
            'chita'       => '661950',
            'krasnoyarsk' => '635320',
            'krasnodar'   => '633540',
            'moscow'      => '637640',
            'piter'       => '653240',
            'barnaul'     => '621630',
            'novosib'     => '641780',
            'novgorod'    => '640860',
            'omsk'        => '640860',
            'kazan'       => '650400',
            'bryansk'     => '623880',
            'astrahan'    => '623130',
            'ulanude'     => '653271',
            'vladimir'    => '624360',
            'kostroma'    => '632490'
        ];

        $currentCity = $cities[$city];

        $lastStamp = [
            '1549785360',
            '1549504285',
            '1548985885',
            '1548381085',
            '1547949085',
            '1547517085',
            '1547085085',
            '1546653085'
        ];

        foreach ($lastStamp as $time) {
            $baseURL =
                Avito::URL_LIST . '&' .
                Avito::FIELD_API_KEY . '=' . Avito::API_KEY . '&' .
                Avito::FIELD_LIMIT . '=' . Avito::LIMIT . '&' .
                Avito::FIELD_CATEGORY . '=' . $category . '&' .
                'lastStamp=' . $time . '&locationId=' . $currentCity;
            $proxy   = new Proxy();
            for ($numberPage = $startPage; $numberPage <= $endPage; $numberPage++) {
                try {
                    $url          = $baseURL . '&' . Avito::FIELD_PAGE . '=' . $numberPage;
                    $guzzleClient = new Client();
                    $resultQuery  = $guzzleClient->request('GET', $url, []);
                    $resultQuery  = \json_decode($resultQuery->getBody()->getContents());
                    $objects      = $resultQuery->result->items;
                    var_dump('СТРАНИЦА ' . $numberPage . '; Объявлений: ' . count($objects));
                    if (count($objects) === 0) {
                        var_dump('СТРАНИЦА ' . $numberPage . '; Объявления кончились ');
                        return;
                    }
                    foreach ($objects as $object) {
                        try {
                            if ($object->type === 'item' || $object->type === 'xlItem')
                                $result = self::createObject($object, $proxy);
                            if ($object->type === 'vip') {
                                foreach ($object->value->list as $item) {
                                    $result = self::createObject($item, $proxy);
                                }
                            }
                        } catch (\Throwable $exception) {
                            Log::warning('Ошибка получения. ' . json_encode($object) . $exception->getMessage());
                        }
                    }
                } catch (GuzzleException $error) {
                    Log::warning('Ошибка получения. ' . $error->getMessage());
                } catch (\Exception $e) {
                    Log::warning('Ошибка получения. ' . $e->getMessage());
                }
            }
        }
    }

    private static function createObject($object, Proxy $proxy = null)
    {
        if ($object->value->category->id !== Avito::CATEGORY_FLATS &&
            $object->value->category->id !== Avito::CATEGORY_ROOMS &&
            $object->value->category->id !== Avito::CATEGORY_HOUSES &&
            $object->value->category->id !== Avito::CATEGORY_LANDS) {
            var_dump('Не обрабатываемая категория: ' . $object->value->category->name);
            return false;
        }

        var_dump($object->value->id);
        if (BaseObject::where('source', '=', 'https://www.avito.ru' . $object->value->uri_mweb)->first()) {
            var_dump('Повторное объявление');
            return false;
        }
        $url_for_object     = Avito::URL_OBJECT . $object->value->id . '?' . Avito::FIELD_API_KEY . '=' . Avito::API_KEY . '&' . Avito::PARAMETRS_FOR_OBJECT;
        $guzzleClientObject = new Client();
        //$resultQuery        = $guzzleClientObject->request('GET', $url_for_object, ['proxy' => $proxy->getProxy()]);
        $resultQuery = $guzzleClientObject->request('GET', $url_for_object, []);
        $objectInfo  = \json_decode($resultQuery->getBody()->getContents());


        var_dump('Разбор объявления');
        $builder = AvitoBuilderObjectFactory::getBuilder($objectInfo);
        if (empty($builder)) {
            var_dump('Не удалось определить билдер для ' . $object->value->uri_mweb);
            return false;
        }
        $object = new BaseObjectFactory($builder);
        var_dump('Объявление добавлено');
        return true;
    }
}