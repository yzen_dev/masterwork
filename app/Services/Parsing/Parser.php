<?php

namespace App\Services\Parsing;


/**
 * Интерфейс для парсинга сайтов
 *
 * @package App\Services
 */
interface Parser
{
    /**
     * Запуск парсера
     *
     * @param  string|null $category
     * @param  string      $city
     *
     * @return void
     */
    static function start($city, $category = null);

    /**
     * Парсинг заданной категории
     *
     * @param              $city
     * @param  string|null $category
     *
     * @return void
     */
    static function parsingCategory($city,$category);

    /**
     * Парсинг всех категорий
     *
     * @param $city
     *
     * @return void
     */
    static function parsingAllCategories($city);
}