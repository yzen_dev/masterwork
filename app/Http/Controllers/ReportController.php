<?php

namespace App\Http\Controllers;

use App\Http\Resources\MarkerResource;
use App\Http\Resources\ObjectResource;
use App\Models\BaseObject;
use App\Models\Catalogs\Category;
use Illuminate\Http\Request;

class ReportController extends Controller
{

    public function flat()
    {
        $objects = BaseObject::where('type', '=', Category::FLAT)
            ->join('flats','id','=','extended_id')
            ->where('city_id', '=', \request('city_id'))
            ->groupBy('flats.count_rooms')
            ->select('count(*),sum(*)')
            ->get();
        return MarkerResource::collection($objects);
    }

}
