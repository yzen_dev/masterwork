<?php

namespace App\Http\Controllers;

use App\Http\Resources\MarkerResource;
use App\Http\Resources\ObjectResource;
use App\Models\BaseObject;
use Illuminate\Http\Request;

class MapController extends Controller
{

    public function index()
    {
        //return MarkerResource::collection(BaseObject::whereRaw('ST_Within(coordinates,ST_Union(' . request()->bounds->northEast .',' .request()->bounds->southWest . '))')->get());
        $coor = request()->bounds['northEast']['lng'] . ' ' . request()->bounds['northEast']['lat'] . ', ' .
            request()->bounds['northEast']['lng'] . ' ' . request()->bounds['southWest']['lat'] . ', ' .
            request()->bounds['southWest']['lng'] . ' ' . request()->bounds['southWest']['lat'] . ', ' .
            request()->bounds['southWest']['lng'] . ' ' . request()->bounds['northEast']['lat'] . ', ' .
            request()->bounds['northEast']['lng'] . ' ' . request()->bounds['northEast']['lat'];
        //return MarkerResource::collection(BaseObject::whereRaw('ST_Within(coordinates,ST_GeometryFromText(\'POLYGON((' . $coor . '))\'))')->get());
        return MarkerResource::collection(BaseObject::limit(100)->get());
    }

}
