<?php

namespace App\Http\Controllers;

use App\Models\BaseObject;
use App\Models\Catalogs\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function PHPSTORM_META\type;

class StatisticsController extends Controller
{
    public function flat()
    {
        $city       = City::where('name', '=', \request()->input('city'))->first();
        $statistics = [];
        $objects    = BaseObject::query()
            ->where('category_id', '=', 1)
            ->with('extended');

        if (!empty(\request()->input('city'))) {
            $objects = $objects->whereHas('building', function ($query) {
                if (!empty($type))
                    $query->where('type_housing_id', '=', (\request()->input('city') === 'Новостройки') ? '3' : '1');
            });
        }

        if (!empty($city)) {
            $objects = $objects->where('city_id', '=', $city->id);
        }

        $objects = $objects->get()->toArray();

        if (!empty(\request()->input('count'))) {
            $count = (int)\request()->input('count');
            foreach ($objects as $key => $object) {
                if ($object['extended']['count_rooms'] !== $count) unset($objects[$key]);
            }
        }

        $objects = array_chunk($objects, count($objects) / 14);
        foreach ($objects as $key => $object) {
            $statistics[$key] = 0;
            foreach ($object as $item) {
                if (isset($item['extended']['area']))
                    $statistics[$key] += $item['price'] / $item['extended']['area'];
            }
            $statistics[$key] = round($statistics[$key] / count($object), 2);
        }

        $sum = 0;
        foreach ($statistics as $statistic) {
            $sum += $statistic;
        }
        $costArea   = round($sum / count($statistics), 2);
        $cost      = $costArea * (float)\request()->input('area');

        return response([
            'city' => $city ?? 'Россия',
            'data' => $statistics,
            'cost' => $cost
        ]);
    }
}
