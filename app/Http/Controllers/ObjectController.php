<?php

namespace App\Http\Controllers;

use App\Http\Resources\ObjectResource;
use App\Models\BaseObject;
use Illuminate\Http\Request;

class ObjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $page = \request()->page;
        return ObjectResource::collection(BaseObject::with('extended')->paginate(26,['*'], 'page',$page));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return ObjectResource
     */
    public function store(Request $request)
    {
        //TODO: Тестовый текс
        $object = BaseObject::create([
            'user_id'     => $request->user()->id,
            'title'       => $request->title,
            'description' => $request->description,
        ]);
        return new ObjectResource($object);
    }

    /**
     * Display the specified resource.
     *
     * @param BaseObject $object
     *
     * @return ObjectResource
     */
    public function show(BaseObject $object)
    {
        return new ObjectResource($object);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param BaseObject                $object
     *
     * @return ObjectResource
     */
    public function update(Request $request, BaseObject $object)
    {
        // check if currently authenticated user is the owner of the book
        if ($request->user()->id !== $object->user_id) {
            return response()->json(['error' => 'Вы не можете изменить данное объявление.'], 403);
        }

        $object->update($request->only(['title', 'description']));

        return new ObjectResource($object);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param BaseObject $object
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(BaseObject $object)
    {
        $object->delete();

        return response()->json(null, 204);
    }
}
