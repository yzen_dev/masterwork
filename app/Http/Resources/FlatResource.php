<?php

namespace App\Http\Resources;

use App\Models\Catalogs\Bathroom;
use App\Models\Catalogs\Repair;
use Illuminate\Http\Resources\Json\JsonResource;

class FlatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'bathroom'       => Bathroom::where('id', '=', $this->bathroom_id)->first()->name ?? null,
            'repair'         => Repair::where('id', '=', $this->repair_id)->first()->name ?? null,
            'count_rooms'    => $this->count_rooms,
            'floor'          => $this->floor,
            'ceiling_height' => $this->ceiling_height,
            'area'           => $this->area,
            'kitchen_space'  => $this->kitchen_space,
            'living_space'   => $this->living_space,
        ];
    }
}
