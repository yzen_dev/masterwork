<?php

namespace App\Http\Resources;

use App\Models\Catalogs\Bathroom;
use App\Models\Catalogs\Repair;
use Illuminate\Http\Resources\Json\JsonResource;

class HouseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'area'               => $this->area,
            'lot_area'           => $this->lot_area,
            'lot_area_unit'      => $this->lot_area_unit,
            'distance_from_city' => $this->distance_from_city,
        ];
    }
}
