<?php

namespace App\Http\Resources;

use App\Models\Catalogs\Bathroom;
use App\Models\Catalogs\Repair;
use Illuminate\Http\Resources\Json\JsonResource;

class LandResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'area'           => $this->lot_area,
            'area_unit'      => $this->lot_area_unit,
        ];
    }
}
