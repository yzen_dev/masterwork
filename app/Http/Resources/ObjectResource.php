<?php

namespace App\Http\Resources;

use App\Factories\ExtendedResourceFactory;
use App\Models\Catalogs\Category;
use App\Models\Catalogs\City;
use Illuminate\Http\Resources\Json\JsonResource;

class ObjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'category'     => Category::getNameCategoryById($this->category_id),
            'type'        => $this->type,
            'price'        => $this->price,
            'description'  => $this->description,
            'coordinates'  => \json_decode($this->coordinates),
            'images'       => \json_decode($this->images),
            'date_created' => $this->created_at,
            'city'         => City::where('id', '=', $this->city_id)->first()->name,
            'fullAddress'  => $this->full_address,
            'extended'     => ExtendedResourceFactory::getExtendedResource($this->extended),
            'user'         => new UserResource($this->user),
            'building'     => new BuildingResource($this->building),
        ];
    }
}
