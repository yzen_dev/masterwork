<?php

namespace App\Http\Resources;

use App\Models\Catalogs\Bathroom;
use App\Models\Catalogs\Repair;
use Illuminate\Http\Resources\Json\JsonResource;

class RoomResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'count_rooms'   => $this->count_rooms,
            'floor'         => $this->floor,
            'area'          => $this->area,
            'room_space'    => $this->room_space,
            'kitchen_space' => $this->kitchen_space,
        ];
    }
}
