<?php

namespace App\Http\Resources;

use App\Models\Catalogs\Bathroom;
use App\Models\Catalogs\BuildingType;
use App\Models\Catalogs\HousingType;
use App\Models\Catalogs\Repair;
use Illuminate\Http\Resources\Json\JsonResource;

class BuildingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'building_type' => BuildingType::where('id','=',$this->building_type_id)->first()->name ?? null,
            'floors_total'  => $this->floors_total,
            'type_housing'  => HousingType::where('id','=',$this->type_housing_id)->first()->name ?? null,
        ];
    }
}
