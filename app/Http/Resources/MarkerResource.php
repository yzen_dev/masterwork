<?php

namespace App\Http\Resources;

use App\Factories\ExtendedResourceFactory;
use App\Models\Catalogs\Category;
use App\Models\Catalogs\City;
use Illuminate\Http\Resources\Json\JsonResource;

class MarkerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'price'        => $this->price,
            'coordinates'  => \json_decode($this->coordinates),
        ];
    }
}
