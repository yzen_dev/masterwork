<?php

namespace App\Models;

/**
 * App\Models\House
 *
 * @property float area                    Площадь дома
 * @property float lot_area                Площадь участка
 * @property float lot_area_unit           Единица измерения
 * @property float distance_from_city      Расстояние от города
 * @property-read \App\Models\BaseObject $baseObject
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\House newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\House newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\House query()
 * @mixin \Eloquent
 */
class House extends AbstractCategoryObject
{
    protected $table = 'houses';
}