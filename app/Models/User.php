<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\User
 *
 * @property integer id         ID Объявления
 * @property string  name       Имя пользователя
 * @property string  phones     Номера телефонов
 * @property string  password   Пароль для авторизации
 * @property string  agency     Названия агенства
 * @property string  created_at Дата создания
 * @property string  updated_at Дата редактирования
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\BaseObject[] $baseObjects
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @mixin \Eloquent
 */
class User extends Model
{
    protected $table = 'users';

    /**
     * Получение объявлений пользователя
     */
    public function baseObjects()
    {
        return $this->belongsToMany('App\Models\BaseObject');
    }
}