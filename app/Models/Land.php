<?php

namespace App\Models;

/**
 * App\Models\Land
 *
 * @property float lot_area                Площадь участка
 * @property float lot_area_unit           Единица измерения
 * @property-read \App\Models\BaseObject $baseObject
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Land newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Land newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Land query()
 * @mixin \Eloquent
 */
class Land extends AbstractCategoryObject
{
    protected $table = 'lands';
}