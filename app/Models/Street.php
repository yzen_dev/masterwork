<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Street
 *
 * @property string  id      ID улицы
 * @property integer name    Название улицы
 * @property integer city_id RID города
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Building[] $buildings
 * @property-read \App\Models\Catalogs\City $city
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Street newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Street newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Street query()
 * @mixin \Eloquent
 */
class Street extends Model
{
    protected $table = 'streets';

    /**
     * Получение зданий на улице
     */
    public function buildings()
    {
        return $this->belongsToMany('App\Models\Building');
    }

    /**
     * Получить город
     */
    public function city()
    {
        return $this->belongsTo('App\Models\Catalogs\City');
    }
}