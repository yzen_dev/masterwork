<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Building
 *
 * @property integer id               ID дома
 * @property integer street_id        ID улицы
 * @property integer district_id      ID района
 * @property integer building_type_id ID тип дома
 * @property integer type_housing_id  ID тип жилья
 * @property integer floors_total     Этажей в доме
 * @property float   built_year       Год сдачи (год постройки)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\BaseObject[] $baseObjects
 * @property-read \App\Models\Catalogs\District $district
 * @property-read \App\Models\Catalogs\HousingType $housingType
 * @property-read \App\Models\Street $street
 * @property-read \App\Models\Catalogs\BuildingType $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Building newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Building newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Building query()
 * @mixin \Eloquent
 */
class Building extends Model
{
    protected $table = 'buildings';

    /**
     * Получение объявления в здание
     */
    public function baseObjects()
    {
        return $this->belongsToMany('App\Models\BaseObject');
    }

    /**
     * Получить типа здания
     */
    public function type()
    {
        return $this->belongsTo('App\Models\Catalogs\BuildingType');
    }

    /**
     * Получение типа жилья
     */
    public function housingType()
    {
        return $this->belongsTo('App\Models\Catalogs\HousingType');
    }

    /**
     * Получить района
     */
    public function district()
    {
        return $this->belongsTo('App\Models\Catalogs\District');
    }

    /**
     * Получить улицы
     */
    public function street()
    {
        return $this->belongsTo('App\Models\Street');
    }
}