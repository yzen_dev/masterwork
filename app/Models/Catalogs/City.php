<?php

namespace App\Models\Catalogs;

use App\Models\BaseObject;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Catalogs\City
 *
 * @property integer                      id         ID города
 * @property string                       name       Название города
 * @property integer                      region_id  ID региона
 * @property Carbon                       updated_at Дата редактирования
 * @property Carbon                       created_at Дата создания
 * @property-read Collection|BaseObject[] $baseObjects
 * @property-read Region                  $region
 * @method static \Illuminate\Database\Eloquent\Builder|City newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|City newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|City query()
 * @mixin \Eloquent
 */
class City extends Model
{
    protected $table = 'catalog_cities';

    protected $fillable = ['name', 'region_id'];

    /**
     * Получение региона
     */
    public function region()
    {
        return $this->belongsTo('App\Models\Catalogs\Region');
    }

    /**
     * Получение объявлений в городе
     */
    public function baseObjects()
    {
        return $this->hasMany('App\Models\BaseObject');
    }
}