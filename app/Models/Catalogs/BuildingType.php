<?php

namespace App\Models\Catalogs;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Catalogs\BuildingType
 *
 * @property integer id         ID типа
 * @property string  name       Название типа дома
 * @property string  created_at Дата создания
 * @property string  updated_at Дата редактирования
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Building[] $building
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalogs\BuildingType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalogs\BuildingType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalogs\BuildingType query()
 * @mixin \Eloquent
 */
class BuildingType extends Model
{
    protected $table = 'catalog_building_types';

    protected $fillable = ['name'];

    /**
     * Получение всех квартиры для данного типа
     */
    public function building()
    {
        return $this->hasMany('App\Models\Building');
    }
}