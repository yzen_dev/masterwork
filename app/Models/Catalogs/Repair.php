<?php

namespace App\Models\Catalogs;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Catalogs\Repair
 *
 * @property integer id         ID вида
 * @property string  name       Название вида ремонта
 * @property string  created_at Дата создания
 * @property string  updated_at Дата редактирования
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Flat[] $flats
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalogs\Repair newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalogs\Repair newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalogs\Repair query()
 * @mixin \Eloquent
 */
class Repair extends Model
{
    protected $table = 'catalog_repairs';

    /**
     * Получение все квартиры для данного типа
     */
    public function flats()
    {
        return $this->hasMany('App\Models\Flat');
    }
}