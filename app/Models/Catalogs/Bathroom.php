<?php

namespace App\Models\Catalogs;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Catalogs\Bathroom
 *
 * @property integer id         ID вида
 * @property string  name       Название вида санузла
 * @property string  created_at Дата создания
 * @property string  updated_at Дата редактирования
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Flat[] $flats
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalogs\Bathroom newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalogs\Bathroom newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalogs\Bathroom query()
 * @mixin \Eloquent
 */
class Bathroom extends Model
{
    protected $table = 'catalog_bathrooms';

    /**
     * Получить все квартиры для данного типа
     */
    public function flats()
    {
        return $this->hasMany('App\Models\Flat');
    }
}