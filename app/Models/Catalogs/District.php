<?php

namespace App\Models\Catalogs;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Catalogs\District
 *
 * @property integer id         ID города
 * @property string  name       Название района
 * @property integer city_id    ID города
 * @property integer up_id      ID родительского района
 * @property string  created_at Дата создания
 * @property string  updated_at Дата редактирования
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\BaseObject[] $baseObjects
 * @property-read \App\Models\Catalogs\City $city
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalogs\District newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalogs\District newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalogs\District query()
 * @mixin \Eloquent
 */
class District extends Model
{
    protected $table = 'catalog_districts';

    /**
     * Получение региона
     */
    public function city()
    {
        return $this->hasOne('App\Models\Catalogs\City');
    }

    /**
     * Получение объявлений в районе
     */
    public function baseObjects()
    {
        return $this->belongsToMany('App\Models\BaseObject');
    }
}