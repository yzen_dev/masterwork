<?php

namespace App\Models\Catalogs;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Catalogs\Category
 *
 * @property integer id         ID категории
 * @property string  name       Название категории
 * @property string  created_at Дата создания
 * @property string  updated_at Дата редактирования
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\BaseObject[] $baseObjects
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalogs\Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalogs\Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalogs\Category query()
 * @mixin \Eloquent
 */
class Category extends Model
{
    protected $table = 'catalog_categories';

    public const FLAT    = 1;
    public const ROOM    = 2;
    public const HOUSE   = 3;
    public const COTTAGE = 4;
    public const DACHA   = 5;
    public const LAND    = 6;
    public const OFFICE  = 7;
    public const GARAGE  = 8;

    /**
     * Получение объявлений по категории
     */
    public function baseObjects()
    {
        return $this->hasMany('App\Models\BaseObject');
    }

    /**
     * @param integer $id ID категории
     *
     * @return string Название категории
     */
    public static function getNameCategoryById($id)
    {
        switch ($id) {
            case 1:
                return 'flat';
            case 2:
                return 'room';
            case 3:
                return 'house';
            case 4:
                return 'cottage';
            case 5:
                return 'dacha';
            case 6:
                return 'land';
            case 7:
                return 'office';
            case 8:
                return 'garage';
            default:
                return 'other';
        }
    }

    /**
     * @param string $class Класс категории
     *
     * @return integer Название категории
     */
    public static function getCategoryIdByClass($class)
    {
        switch ($class) {
            case 'App\Models\Flat':
                return 1;
            case 'App\Models\Room':
                return 2;
            case 'App\Models\House':
                return 3;
            case 'App\Models\Cottage':
                return 4;
            case 4:
                return 4;
            case 5:
                return 5;
            case 'App\Models\Land':
                return 6;
            case 7:
                return 7;
            default:
                return 777;
        }
    }
}