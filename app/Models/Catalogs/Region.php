<?php

namespace App\Models\Catalogs;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Catalogs\Region
 *
 * @property integer   id         ID категории
 * @property string    name       Название региона
 * @property integer   code       Код региона
 * @property string    created_at Дата создания
 * @property string    updated_at Дата редактирования
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Catalogs\City[] $cities
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalogs\Region newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalogs\Region newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalogs\Region query()
 * @mixin \Eloquent
 */
class Region extends Model
{
    protected $table = 'catalog_regions';

    /**
     * Получение городов региона
     */
    public function cities()
    {
        return $this->hasMany('App\Models\Catalogs\City');
    }
}