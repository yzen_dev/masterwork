<?php

namespace App\Models\Catalogs;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Catalogs\HousingType
 *
 * @property integer id         ID вида
 * @property string  name       Название вида
 * @property string  created_at Дата создания
 * @property string  updated_at Дата редактирования
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Building[] $building
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalogs\HousingType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalogs\HousingType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Catalogs\HousingType query()
 * @mixin \Eloquent
 */
class HousingType extends Model
{
    protected $table = 'catalog_types_housing';

    protected $fillable = ['name'];

    /**
     * Получение всех домов для данного типа
     */
    public function building()
    {
        return $this->hasMany('App\Models\Building');
    }

}