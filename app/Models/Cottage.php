<?php

namespace App\Models;

/**
 * App\Models\Cottage
 *
 * @property float area                    Площадь дома
 * @property float lot_area                Площадь участка
 * @property float lot_area_unit           Единица измерения
 * @property float distance_from_city      Расстояние от города
 * @property-read \App\Models\BaseObject $baseObject
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cottage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cottage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cottage query()
 * @mixin \Eloquent
 */
class Cottage extends AbstractCategoryObject
{
    protected $table = 'cottages';
}