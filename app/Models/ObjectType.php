<?php

namespace App\Model\Object;


class ObjectType
{
    /**
     * @var int  Продажа
     */
    public const  SALE = 1;

    /**
     * @var  int  Аренда
     */
    public const LEASE = 2;

    /**
     * @var  array  Соответсвие Id типа обьявления его названию
     */
    public const OBJECT_TYPES_NAMES = [
        1 => 'Продажа',
        2 => 'Аренда'
    ];
}