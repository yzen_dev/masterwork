<?php

namespace App\Models;

/**
 * App\Models\Room
 *
 * @property integer floor               Этаж квартиры
 * @property integer count_rooms         Количество комнат в квартире
 * @property float   room_space          Площадь комнаты
 * @property float   area                Общая площадь
 * @property float   kitchen_space       Площадь кухни
 * @property boolean studio              Студия
 * @property-read \App\Models\BaseObject $baseObject
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Room newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Room newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Room query()
 * @mixin \Eloquent
 */
class Room extends AbstractCategoryObject
{
    protected $table = 'rooms';
}