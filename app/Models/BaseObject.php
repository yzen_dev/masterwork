<?php

namespace App\Models;


use Carbon\Carbon;
use App\Models\Catalogs\City;
use App\Models\Catalogs\Category;
use App\Models\Catalogs\District;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BaseObject
 *
 * @property integer                                            id            ID Объявления
 * @property integer                                            user_id       ID пользователя
 * @property integer                                            category_id   ID категории
 * @property integer                                            city_id       ID города
 * @property integer                                            district_id   ID района
 * @property integer                                            building_id   ID Здания
 * @property integer                                            term          Срок аренды
 * @property integer                                            type          Тип сделки
 * @property string                                             source        Откуда объявление
 * @property string                                             full_address  Полный адрес объекта
 * @property string                                             description   Описание объявления
 * @property string                                             coordinates   Координаты объекта
 * @property float                                              price         Стоимость объекта
 * @property array                                              images        Массив изображений
 * @property Carbon                                             created_at    Дата создания
 * @property Carbon                                             updated_at    Дата редактирования
 * @property integer                                            extended_id   ID подробной информации
 * @property string                                             extended_type Класс объявления
 * @property-read Building                                      $building
 * @property-read Category                                      $category
 * @property-read City                                          $city
 * @property-read District                                      $district
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $extended
 * @property-read User                                          $user
 * @method static \Illuminate\Database\Eloquent\Builder|BaseObject newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BaseObject newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BaseObject query()
 * @mixin \Eloquent
 */
class BaseObject extends Model
{
    protected $table = 'base_objects';

    public function extended()
    {
        return $this->morphTo();
    }

    /**
     * Получить здание
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Получить здание
     */
    public function building()
    {
        return $this->belongsTo('App\Models\Building');
    }

    /**
     * Получить категорию объявления
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Catalogs\Category');
    }

    /**
     * Получить города
     */
    public function city()
    {
        return $this->belongsTo('App\Models\Catalogs\City');
    }

    /**
     * Получить района
     */
    public function district()
    {
        return $this->belongsTo('App\Models\Catalogs\District');
    }

}