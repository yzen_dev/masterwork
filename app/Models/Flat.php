<?php

namespace App\Models;

use App\Models\Catalogs\Bathroom;
use App\Models\Catalogs\Repair;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\Flat
 *
 * @property integer         floor               Этаж квартиры
 * @property integer         type_of_bathroom_id Тип санузла
 * @property integer         type_repairs_id     Тип ремонта
 * @property integer         count_rooms         Скольки комнатная квартира
 * @property float           ceiling_height      Высота потолков
 * @property float           area                Общая площадь
 * @property float           kitchen_space       Площадь кухни
 * @property float           living_space        Жилая площадь
 * @property boolean         studio              Студия
 * @property-read BaseObject $baseObject         Основная информаци о объявление
 * @property-read Bathroom   $bathroom           Вид санузала
 * @property-read Repair     $repairs            Вид ремонта
 * @method static Builder|Flat newModelQuery()
 * @method static Builder|Flat newQuery()
 * @method static Builder|Flat query()
 * @mixin \Eloquent
 */
class Flat extends AbstractCategoryObject
{
    protected $table = 'flats';

    /**
     * Получить вид санузла
     */
    public function bathroom()
    {
        return $this->belongsTo('App\Models\Catalogs\Bathroom');
    }

    /**
     * Получить вид ремонта
     */
    public function repairs()
    {
        return $this->belongsTo('App\Models\Catalogs\Repair');
    }
}