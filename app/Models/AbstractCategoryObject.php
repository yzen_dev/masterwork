<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id ID Подробной информации
 */
abstract class AbstractCategoryObject extends Model
{
    public $timestamps = false;

    /**
     * Получить основную информацию
     */
    public function baseObject()
    {
        return $this->morphOne('App\Models\BaseObject', 'extended');
    }
}