<?php

namespace App\Models\Sites;

/**
 * Класс для сайта Циан
 *
 * @package App\Services
 */
class Cian
{
    /**
     * Категории недвижимости - Квартиры
     */
    public const CATEGORY_FLATS = 'kupit-kvartiru';

    /**
     * Категории недвижимости - Комнаты
     */
    public const CATEGORY_ROOMS = 'kupit-komnatu';

    /**
     * Категории недвижимости - Дома
     */
    public const CATEGORY_HOUSES = 'kupit-dom';

    /**
     * Категории недвижимости - Земельные участки
     */
    public const CATEGORY_LANDS = 'kupit-zemelniy-uchastok';

    /**
     * Категории недвижимости - Гараж
     */
    public const CATEGORY_GARAGES = 'kupit-garazh';

    /**
     * Категории недвижимости - Офис
     */
    public const CATEGORY_OFFICE = 'kupit-ofis';

    /**
     * Список категорий для парсинга
     */
    public const CATEGORIES = [
        self::CATEGORY_FLATS,
        self::CATEGORY_ROOMS,
        self::CATEGORY_HOUSES,
        self::CATEGORY_LANDS,
        self::CATEGORY_GARAGES,
        self::CATEGORY_OFFICE,
    ];
}