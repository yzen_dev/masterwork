<?php

namespace App\Models\Sites;

/**
 * Класс для сайта Авито
 *
 * @package App\Services
 */
class Avito
{

    /**
     * Ссылка на список объявлений
     *
     * @var string
     */
    public const URL_LIST = 'https://m.avito.ru/api/9/items?lastStamp=1544711580&display=list&locationId=621540';

    /**
     * Ссылка на объявление
     *
     * @var string
     */
    public const URL_OBJECT = 'https://m.avito.ru/api/8/items/';

    /**
     * Название поля номера ключа
     *
     * @var string
     */
    public const FIELD_API_KEY = 'key';

    /**
     * Ключ для доступа к API
     *
     * @var string
     */
    public const API_KEY = 'af0deccbgcgidddjgnvljitntccdduijhdinfgjgfjir';

    /**
     * Название поля номера страницы
     *
     * @var string
     */
    public const FIELD_PAGE = 'page';

    /**
     * Название поля количества отображаемых объявлений
     *
     * @var string
     */
    public const FIELD_LIMIT = 'limit';

    /**
     * Количество запрашиваемых объявлений
     *
     * @var string
     */
    public const LIMIT = '100';

    /**
     * Параметры для получения информации о объекте
     *
     * @var string
     */
    public const PARAMETRS_FOR_OBJECT = 'includeRefs=1';

    /**
     * Название поля категории отображаемых объявлений
     *
     * @var string
     */
    public const FIELD_CATEGORY = 'categoryId';

    /**
     * Категория получения всеъ объявлений
     *
     * @var string
     */
    public const CATEGORY_ALL = 4;

    /**
     * Категории недвижимости - Квартиры
     *
     * @var string
     */
    public const CATEGORY_FLATS = 24;

    /**
     * Категории недвижимости - Комнаты
     *
     * @var string
     */
    public const CATEGORY_ROOMS = 23;

    /**
     * Категории недвижимости - Дома,Дачи и коттеджи
     *
     * @var string
     */
    public const CATEGORY_HOUSES = 25;

    /**
     * Категории недвижимости - Земельные участки
     *
     * @var string
     */
    public const CATEGORY_LANDS = 26;

    /**
     * Категории недвижимости - Гаражи и машиноместа
     *
     * @var string
     */
    public const CATEGORY_GARAGES = 85;

    /**
     * Категории недвижимости - Коммерческая недвижимость
     *
     * @var string
     */
    public const CATEGORY_COMMERCIAL_REAL = 42;

    /**
     * Список категорий для парсинга
     *
     * @var array
     */
    public const CATEGORIES = [
        self::CATEGORY_FLATS,
        self::CATEGORY_ROOMS,
        self::CATEGORY_HOUSES,
        self::CATEGORY_LANDS,
        self::CATEGORY_GARAGES,
        self::CATEGORY_COMMERCIAL_REAL,
    ];

}