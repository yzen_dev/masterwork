<?php

namespace App\Models\Sites;

/**
 * Класс для сайта ДомКлик
 *
 * @package App\Services
 */
class DomClick
{
    /**
     * Ссылка на список объявлений
     *
     * @var string
     */
    public const URL_LIST = 'https://offers-service.domclick.ru/research/v1/offers/?address=eebc9cf5-35d0-4191-afe8-fe1c215f06c0';

    /**
     * Категория получения всеъ объявлений
     *
     * @var string
     */
    public const CATEGORY_ALL = '';

    /**
     * Категории недвижимости - Квартиры
     *
     * @var string
     */
    public const CATEGORY_FLATS = 'offer_type=flat';

    /**
     * Категории недвижимости - Квартиры в новостройке
     *
     * @var string
     */
    public const CATEGORY_COMPLEX = 'offer_type=complex';

    /**
     * Категории недвижимости - Комнаты
     *
     * @var string
     */
    public const CATEGORY_ROOMS = 'offer_type=room';

    /**
     * Категории недвижимости - Дома
     *
     * @var string
     */
    public const CATEGORY_HOUSES = 'offer_type=house';

    /**
     * Категории недвижимости - Земельные участки
     *
     * @var string
     */
    public const CATEGORY_LANDS = 'offer_type=lot';

    /**
     * Категории недвижимости - Земельные таунхаус
     *
     * @var string
     */
    public const CATEGORY_TOWNHOUSE = 'offer_type=townhouse';

    /**
     * Категории недвижимости - Часть дома
     *
     * @var string
     */
    public const CATEGORY_HOUSE_PART = 'offer_type=house_part';

    /**
     * Количество запрашиваемых объявлений
     *
     * @var string
     */
    public const LIMIT = 30;

    public const CATEGORIES = [
        self::CATEGORY_FLATS,
        self::CATEGORY_COMPLEX,
        self::CATEGORY_ROOMS,
        self::CATEGORY_HOUSES,
        self::CATEGORY_LANDS,
        self::CATEGORY_TOWNHOUSE,
        self::CATEGORY_HOUSE_PART,
    ];
}