<?php
/**
 * Created by PhpStorm.
 * User: DNS
 * Date: 19.01.2019
 * Time: 19:48
 */

namespace App\Builders;


class PrivateHouseAvitoBuilder
{
    public static function buildPrivateHouse($objectInfo)
    {
        $title = explode(' ', $objectInfo->title)[0];
        switch ($title) {
            case 'Дом':
                return new HouseBuilder($objectInfo, 'AVITO');
            case 'Коттедж':
                return new CottageBuilder($objectInfo, 'AVITO');
        }
    }
}