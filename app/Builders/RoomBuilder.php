<?php

namespace App\Builders;


use App\Models\Room;

class RoomBuilder extends BaseObjectBuilder
{
    /**
     * Создаёт объявление
     *
     * @return void
     */
    public function buildObject()
    {
        switch ($this->site){
            case 'AVITO':
                $this->buildRoomAvito();
                break;
        }
        parent::buildObject();
    }

    private function buildRoomAvito(){
        $this->extendedObject = new Room();

        preg_match('/Комната[\s](\d*[.,]?\d*)[\s]м²/m', $this->fullInformation->title, $room);
        $this->extendedObject->room_space = $room[1];
        preg_match('/в\s(>?\s?\d*)-к/m', $this->fullInformation->title, $rooms);
        $this->extendedObject->count_rooms = ($rooms[1] === '> 9') ? 90 : $rooms[1];

        preg_match('/(\d*)\/\d* эт./m', $this->fullInformation->title, $floors);
        $this->extendedObject->floor = $floors[1];
        $this->extendedObject->save();
    }
}