<?php
/**
 * Created by PhpStorm.
 * User: DNS
 * Date: 03.01.2019
 * Time: 17:26
 */

namespace App\Builders;


use App\Model\Object\ObjectType;
use App\Models\AbstractCategoryObject;
use App\Models\BaseObject;
use App\Models\Catalogs\Category;
use App\Models\Catalogs\City;
use App\Models\User;
use Hash;
use ReflectionClass;

abstract class BaseObjectBuilder
{
    /**
     * @var BaseObject
     */
    protected $object;

    /**
     * @var AbstractCategoryObject
     */
    protected $extendedObject;

    protected $fullInformation;
    protected $site;

    /**
     * @param $fullInformation
     * @param $site
     */
    public function __construct($fullInformation, $site)
    {
        $this->fullInformation = $fullInformation;
        $this->site            = $site;
    }

    /**
     * Возвращает созданное объявление
     *
     * @return BaseObject
     */
    final public function getObject()
    {
        return $this->object;
    }

    /**
     * Создаёт объявление
     *
     * @return void
     */
    public function buildObject()
    {
        switch ($this->site) {
            case 'AVITO':
                $this->buildObjectAvito();
                break;
        }
    }

    /**
     * Создаёт объявление c сайта Авито
     *
     * @return void
     */
    public function buildObjectAvito()
    {
        $this->object               = new BaseObject();
        $this->object->term         = str_replace('руб.', '', $this->fullInformation->price->metric) ?? null;
        $this->object->source       = $this->fullInformation->sharing->url;
        $this->object->full_address = $this->fullInformation->address;
        $this->object->description  = $this->fullInformation->description;
        $this->object->price        = (float)str_replace(' ', '', $this->fullInformation->price->value);
        foreach ($this->fullInformation->parameters->flat as $item) {
            if ($item->title === 'Тип объявления') {
                $this->object->type = ($item->description === 'Продам') ? ObjectType::SALE : ObjectType::LEASE;
            }
        }
        $this->object->coordinates = \json_encode([
            'latitude'  => $this->fullInformation->coords->lat,
            'longitude' => $this->fullInformation->coords->lng
        ]);
        $images                    = [];
        if (!empty($this->fullInformation->images)) {
            foreach ($this->fullInformation->images as $image) {
                $atr140x105 = '140x105';
                $atr640x480 = '640x480';
                $images []  = [
                    '140x105' => $image->$atr140x105,
                    '640x480' => $image->$atr640x480
                ];
            }
        }
        $this->object->images = \json_encode($images);

        /** @var City $city */
        foreach ($this->fullInformation->refs->locations as $location) {
            $city = City::where('name', '=', $location->name)->first();
            if ($city) break;
        }
        if (!$city) {
            $addresses = explode(',', $this->fullInformation->address);
            foreach ($addresses as $location) {
                $city = City::where('name', '=', ltrim($location))->first();
                if ($city) break;
            }
        }
        if ($city) {
            $this->object->city_id = $city->id;
        } else {
            $this->object->city_id = City::where('name', '=', 'Не определен')->first()->id;
        }
        $this->object->extended_id   = $this->extendedObject->id;
        $this->object->extended_type = get_class($this->extendedObject);
        try {
            $user = new User();
            if ($this->fullInformation->seller->title === 'Арендодатель') {
                $user->name   = $this->fullInformation->seller->manager ?? 'Агент';
                $user->agency = $this->fullInformation->seller->name;
            } else {
                $user->name = $this->fullInformation->seller->name;
            }
            $phones = [];
            foreach ($this->fullInformation->contacts->list as $contact) {
                if ($contact->type === 'phone') {
                    preg_match('/number=%2B(\d*)/m', $contact->value->uri, $phone);
                    $phones [] = $phone[1];
                }
            }
            $user->password = Hash::make($phones[0]);
            $user->phones   = json_encode($phones);
            $user->save();
            $this->object->user_id     = $user->id;
        }
        catch (\Throwable $exception){
            var_dump('Не удалось создать пользователя');
            var_dump($this->fullInformation);
            return false;
        }
        $this->object->category_id = Category::getCategoryIdByClass($this->object->extended_type);
        if ($this->object->category_id !== 6) {
            $building = new BuildingBuilder($this->fullInformation, 'AVITO');
            $building->buildBuilding();
            $building = $building->getBuilding();
            $this->object->building_id = $building->id;
        }
        $this->object->save();
    }

}