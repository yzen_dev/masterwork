<?php

namespace App\Builders;


use App\Models\Building;
use App\Models\Catalogs\BuildingType;
use App\Models\Catalogs\HousingType;

class BuildingBuilder
{
    /**
     * @var Building
     */
    protected $building;

    protected $site;

    protected $fullInformation;

    public function __construct($fullInformation, $site)
    {
        $this->fullInformation = $fullInformation;
        $this->site            = $site;
    }

    /**
     * Создаёт объявление
     *
     * @return void
     */
    public function buildBuilding()
    {
        switch ($this->site) {
            case 'AVITO':
                $this->buildBuildingAvito();
                break;
        }
    }

    /**
     * Создаёт объявление c сайта Авито
     *
     * @return void
     */
    public function buildBuildingAvito()
    {
        $title = explode(' ', $this->fullInformation->title)[0];
        switch ($title) {
            case 'Дом':
            case 'Коттедж':
                $this->buildPrivateBuilding();
                break;
            default:
                $this->buildApartamentBuilding();
                break;
        }
    }

    private function buildApartamentBuilding()
    {
        $this->building = new Building();
        preg_match('/\d*\/(\d*) эт./m', $this->fullInformation->title, $floors);
        $this->building->floors_total = $floors[1];
        foreach ($this->fullInformation->parameters->flat as $item) {
            if ($item->title === 'Тип дома')
                $this->building->building_type_id = BuildingType::firstOrCreate(['name' => $item->description])->id;

            if ($item->title === 'Вид объекта')
                $this->building->type_housing_id = HousingType::firstOrCreate(['name' => $item->description])->id;
        }

        $this->building->save();
    }

    private function buildPrivateBuilding()
    {
        try {
            $this->building = new Building();
            foreach ($this->fullInformation->parameters->flat as $item) {
                if ($item->title === 'Материал стен')
                    $this->building->building_type_id = BuildingType::firstOrCreate(['name' => $item->description])->id;
                if ($item->title === 'Этажей в доме')
                    $this->building->floors_total = ($item->description !== '> 4') ? $item->description : '40';
            }
            $this->building->save();
        } catch (\Throwable $exception) {
            var_dump($this->fullInformation);
            dd($exception);
        }
    }

    /**
     * Возвращает созданное здание
     *
     * @return Building
     */
    final public function getBuilding()
    {
        return $this->building;
    }
}