<?php

namespace App\Builders;


use App\Models\Flat;

class FlatBuilder extends BaseObjectBuilder
{
    /**
     * Создаёт объявление
     *
     * @return void
     */
    public function buildObject()
    {
        switch ($this->site){
            case 'AVITO':
                $this->buildFlatAvito();
                break;
        }
        parent::buildObject();
    }

    /**
     *
     */
    private function buildFlatAvito(){
        $this->extendedObject = new Flat();

        preg_match('/(\d*)-к квартира/m', $this->fullInformation->title, $rooms);
        if (!empty($rooms)){
            $this->extendedObject->count_rooms = $rooms[1];
        }
        else{
            preg_match('/(Студия)/m', $this->fullInformation->title, $studio);
            if($studio){
                $this->extendedObject->count_rooms = 1;
                $this->extendedObject->studio = true;
            }
        }

        preg_match('/(\d*)\/\d* эт./m', $this->fullInformation->title, $floors);
        $this->extendedObject->floor = $floors[1];
        preg_match('/,[\s](\d*[.,]?\d*)[\s]м²/m', $this->fullInformation->title, $area);
        $this->extendedObject->area = $area[1];

        foreach ($this->fullInformation->parameters->flat as $item) {
            if ($item->title === 'Жилая площадь, м²') $this->extendedObject->living_space = $item->description;
            if ($item->title === 'Площадь кухни, м²') $this->extendedObject->kitchen_space = $item->description;
        }
        $this->extendedObject->save();
    }
}