<?php

namespace App\Builders;


use App\Models\House;
use App\Models\Land;

class LandBuilder extends BaseObjectBuilder
{
    /**
     * Создаёт объявление
     *
     * @return void
     */
    public function buildObject()
    {
        switch ($this->site) {
            case 'AVITO':
                $this->buildHouseAvito();
                break;
        }
        parent::buildObject();
    }

    private function buildHouseAvito()
    {
        $this->extendedObject = new Land();
        preg_match('/Участок\s(\d*[.,]?\d*)\s/', $this->fullInformation->title, $area);
        $this->extendedObject->area = $area[1];
        //Участок 13 сот.
        preg_match('/Участок\s\d*[.,]?\d*\s(сот|га)/', $this->fullInformation->title, $lot_area_unit);
        $this->extendedObject->area_unit = $lot_area_unit[1];

        foreach ($this->fullInformation->parameters->flat as $item) {
            if ($item->title === 'Расстояние до города, км')
                $this->extendedObject->distance_from_city = ($item->description === 'В черте города') ? 0 : $item->description;
        }

        $this->extendedObject->save();
    }
}