<?php

namespace App\Builders;


use App\Models\Cottage;

class CottageBuilder extends BaseObjectBuilder
{
    /**
     * Создаёт объявление
     *
     * @return void
     */
    public function buildObject()
    {
        switch ($this->site){
            case 'AVITO':
                $this->buildCottageAvito();
                break;
        }
        parent::buildObject();
    }

    private function buildCottageAvito(){

        $this->extendedObject = new Cottage();
        preg_match('/Коттедж\s(\d*[.,]?\d*)\sм²/', $this->fullInformation->title, $area);
        $this->extendedObject->area = $area[1];
        preg_match('/на участке\s(\d*[.,]?\d*)/', $this->fullInformation->title, $lot_area);
        $this->extendedObject->lot_area = $lot_area[1];
        preg_match('/на участке\s\d*[.,]?\d*\s(сот|га)/', $this->fullInformation->title, $lot_area_unit);
        $this->extendedObject->lot_area_unit = $lot_area_unit[1];

        foreach ($this->fullInformation->parameters->flat as $item) {
            if ($item->title === 'Расстояние до города, км')
                $this->extendedObject->distance_from_city = ($item->description === 'В черте города') ? 0 : $item->description;
        }

        $this->extendedObject->save();
    }
}