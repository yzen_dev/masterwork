<?php

namespace App\Console\Commands;

use App\Services\Parsing\ParserAvito;
use App\Services\Parsing\ParserJKH;
use Illuminate\Console\Command;

class ParsingJKH extends Command
{
    /**
     * Имя и аргументы консольной команды.
     *
     * @var string
     */
    protected $signature = 'parsing-site:jkh';

    /**
     * Описание консольной команды.
     *
     * @var string
     */
    protected $description = 'Парсинг объектов недвижимости с Авито';

    /**
     * Создание нового экземпляра команды.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Выполнение консольной команды.
     *
     * @return mixed
     */
    public function handle()
    {
        ParserJKH::start();
    }
}
