<?php

namespace App\Console\Commands;

use App\Services\Parsing\ParserAvito;
use Illuminate\Console\Command;

class ParsingAvito extends Command
{
    /**
     * Имя и аргументы консольной команды.
     *
     * @var string
     */
    protected $signature = 'parsing-site:avito {city} {category=all}';

    /**
     * Описание консольной команды.
     *
     * @var string
     */
    protected $description = 'Парсинг объектов недвижимости с Авито';

    /**
     * Создание нового экземпляра команды.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Выполнение консольной команды.
     *
     * @return mixed
     */
    public function handle()
    {
        ParserAvito::start($this->argument('city'), $this->argument('category'));
    }
}
