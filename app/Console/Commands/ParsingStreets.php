<?php

namespace App\Console\Commands;

use App\Models\Catalogs\City;
use App\Models\Street;
use App\Services\Parsing\ParserAvito;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use PHPHtmlParser\Dom;

class ParsingStreets extends Command
{
    /**
     * Имя и аргументы консольной команды.
     *
     * @var string
     */
    protected $signature = 'parsing:street';

    /**
     * Описание консольной команды.
     *
     * @var string
     */
    protected $description = 'Парсинг объектов недвижимости с Авито';

    /**
     * Создание нового экземпляра команды.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Выполнение консольной команды.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $domLink = new Dom;
            $domLink->loadFromUrl('https://bestmaps.ru/goroda');
            $links = $domLink->find('.tGeoGibrid');
            foreach ($links as $key => $link) {
                if ($key < 2) continue;
                if ($key > 156) break;

                $cityName = $link->find('span')[0]->find('a')->innerHtml;
                $cityLink = $link->find('span')[0]->find('a')->getAttribute('href');
                $cityLink = str_replace('goroda', 'city', $cityLink) . '/street';
                $city = City::whereRaw('lower(name)=\'' . mb_strtolower ($cityName) . '\'')->first();
                if (!$city) {
                    var_dump('Город ' . $cityName . ' не найден');
                    continue;
                }
                var_dump('Добавление улиц города ' . $cityName);

                $domCity = new Dom;
                $domCity->loadFromUrl('https://bestmaps.ru' . $cityLink);
                $streets = $domCity->find('p');
                foreach ($streets as $street) {
                    $newStreet          = new Street();
                    $newStreet->name    = $street->find('a')->innerHtml;
                    $newStreet->city_id = $city->id;
                    $newStreet->save();
                }
            }
        } catch (GuzzleException $error) {
            dd($error);
        }
    }
}
