<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ParsingCian extends Command
{
    /**
     * Имя и аргументы консольной команды.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * Описание консольной команды.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Создание нового экземпляра команды.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Выполнение консольной команды.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    }
}
