<?php

namespace App\Console\Commands;

use App\Services\Parsing\ParserDomClick;
use Illuminate\Console\Command;

class ParsingDomClick extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parsing-site:domclick {category=all}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ParserDomClick::start($this->argument('category'));
    }
}
