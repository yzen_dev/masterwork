<?php

namespace App\Factories;


use App\Models\Sites\Avito;
use App\Builders\FlatBuilder;
use App\Builders\LandBuilder;
use App\Builders\RoomBuilder;
use App\Builders\BaseObjectBuilder;
use App\Builders\PrivateHouseAvitoBuilder;

abstract class AvitoBuilderObjectFactory
{
    /**
     * @var BaseObjectBuilder
     */
    private $builder;

    /**
     * @param $objectInfo
     *
     * @return
     */
    public static function getBuilder($objectInfo)
    {
        switch ($objectInfo->categoryId) {
            case Avito::CATEGORY_FLATS:
                return new FlatBuilder($objectInfo, 'AVITO');
            case Avito::CATEGORY_ROOMS:
                return new RoomBuilder($objectInfo, 'AVITO');
            case Avito::CATEGORY_HOUSES:
                return PrivateHouseAvitoBuilder::buildPrivateHouse($objectInfo);
            case Avito::CATEGORY_LANDS:
                return new LandBuilder($objectInfo, 'AVITO');
        }
    }


}