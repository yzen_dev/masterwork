<?php

namespace App\Factories;


use App\Http\Resources\FlatResource;
use App\Http\Resources\HouseResource;
use App\Http\Resources\RoomResource;
use App\Models\Catalogs\Category;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ExtendedResourceFactory
 *
 * @package App\Factories
 */
class ExtendedResourceFactory
{
    /**
     * @param $category
     *
     * @param $extended
     *
     * @return JsonResource
     */
    public static function getExtendedResource($extended)
    {
        switch ($extended->baseObject->category_id) {
            case Category::FLAT:
                return new FlatResource($extended);
                break;
            case Category::ROOM:
                return new RoomResource($extended);
                break;
            case Category::HOUSE:
                return new HouseResource($extended);
                break;
            case Category::COTTAGE:
                return new HouseResource($extended);
                break;
        }
    }
}