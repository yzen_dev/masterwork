<?php

namespace App\Factories;


use App\Builders\BaseObjectBuilder;
use App\Models\BaseObject;

class BaseObjectFactory
{
    /**
     * @var BaseObjectBuilder
     */
    private $builder;


    /**
     * @param BaseObjectBuilder $builder
     */
    public function __construct(BaseObjectBuilder $builder)
    {
        $this->builder = $builder;
        $this->builder->buildObject();
    }

    /**
     * Возвращает созданное объявление
     *
     * @return BaseObject
     */
    public function getObject()
    {
        return $this->builder->getObject();
    }
}