<?php

Route::apiResource('/objects', 'ObjectController');

Route::prefix('map')->group(function () {
    Route::post('objects', 'MapController@index');
});

Route::prefix('report')->group(function () {
    Route::post('flats', 'ReportController@flat');
});

Route::get('statistics', 'StatisticsController@flat');
