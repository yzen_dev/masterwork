<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Catalogs\District::class, function (Faker $faker) {
    return [
        'name'          => 'Центральный район',
        'city_id'          => function () {
            return factory(App\Models\Catalogs\City::class)->create()->id;
        },
        'up_id'         => 0,
        'created_at'    => now(),
        'updated_at'    => now()
    ];
});
