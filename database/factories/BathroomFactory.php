<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Catalogs\Bathroom::class, function (Faker $faker) {
    return [
        'name'          => 'Раздельный',
        'created_at'    => now(),
        'updated_at'    => now()
    ];
});
