<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Room::class, function (Faker $faker) {
    return [
        'floor'                => $faker->randomDigitNotNull,
        'count_rooms'  => $faker->randomDigitNotNull,
        'room_space'       => $faker->randomFloat,
        'area'                 => $faker->randomFloat,
        'kitchen_space'        => $faker->randomFloat
    ];
});
