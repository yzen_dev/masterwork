<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Building::class, function (Faker $faker) {
    return [
        'number'          => '123',
        'street_id'          => function () {
            return factory(App\Models\Street::class)->create()->id;
        },
        'building_type_id'          => function () {
            return factory(App\Models\Catalogs\BuildingType::class)->create()->id;
        },
        'type_housing_id'          => function () {
            return factory(App\Models\Catalogs\HousingType::class)->create()->id;
        },
        'floors_total'         => $faker->randomDigitNotNull,
        'built_year'         => $faker->randomDigitNotNull,
        'created_at'      => now(),
        'updated_at'      => now()
    ];
});
