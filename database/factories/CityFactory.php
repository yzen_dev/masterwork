<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Catalogs\City::class, function (Faker $faker) {
    return [
        'name'          => 'Чита',
        'region_id'          => function () {
            return factory(App\Models\Catalogs\Region::class)->create()->id;
        },
        'created_at'    => now(),
        'updated_at'    => now()
    ];
});
