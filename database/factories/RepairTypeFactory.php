<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Catalogs\Repair::class, function (Faker $faker) {
    return [
        'name'          => 'Евроремонт',
        'created_at'    => now(),
        'updated_at'    => now()
    ];
});
