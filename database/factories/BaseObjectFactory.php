<?php

use Faker\Generator as Faker;

$factory->define(App\Models\BaseObject::class, function (Faker $faker) {
    return [
        'user_id'          => function () {
            return factory(App\Models\User::class)->create()->id;
        },
        'category_id'      => function () {
            return factory(App\Models\Catalogs\Category::class)->create()->id;
        },
        'city_id'          => function () {
            return factory(App\Models\Catalogs\City::class)->create()->id;
        },
        'district_id'      => function () {
            return factory(App\Models\Catalogs\District::class)->create()->id;
        },
        'building_id'      => function () {
            return factory(App\Models\Building::class)->create()->id;
        },
        'type'             => \App\Model\Object\ObjectType::SALE,
        'full_address'          => $faker->address,
        'description'      => $faker->name,
        'price'            => $faker->randomFloat,
        'coordinates'      => \json_encode([
            'latitude'   => $faker->latitude,
            'longitude'  => $faker->longitude,
        ]),
        'images'           => \json_encode([]),
        'created_at'       => now(),
        'updated_at'       => now(),
    ];
});
