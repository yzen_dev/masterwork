<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Catalogs\Category::class, function (Faker $faker) {
    return [
        'name'          => 'Квартира',
        'created_at'    => now(),
        'updated_at'    => now()
    ];
});
