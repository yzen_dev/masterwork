<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Catalogs\BuildingType::class, function (Faker $faker) {
    return [
        'name'          => 'Кирпичный',
        'created_at'    => now(),
        'updated_at'    => now()
    ];
});
