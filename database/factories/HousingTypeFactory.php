<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Catalogs\HousingType::class, function (Faker $faker) {
    return [
        'name'          => 'Вторичка',
        'created_at'    => now(),
        'updated_at'    => now()
    ];
});
