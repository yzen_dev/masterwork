<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Catalogs\Region::class, function (Faker $faker) {
    return [
        'name'          => 'Забайкальский край',
        'code'          => '75',
        'created_at'    => now(),
        'updated_at'    => now()
    ];
});
