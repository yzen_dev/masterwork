<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Street::class, function (Faker $faker) {
    return [
        'name'          => 'Ленина',
        'city_id'          => function () {
            return factory(App\Models\Catalogs\City::class)->create()->id;
        },
        'created_at'    => now(),
        'updated_at'    => now()
    ];
});
