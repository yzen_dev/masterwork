<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Flat::class, function (Faker $faker) {
    return [
        'floor'                => $faker->randomDigitNotNull,
        'bathroom_id'  => function () {
            return factory(App\Models\Catalogs\Bathroom::class)->create()->id;
        },
        'repairs_id'      => function () {
            return factory(App\Models\Catalogs\Repair::class)->create()->id;
        },
        'ceiling_height'       => $faker->randomFloat,
        'area'                 => $faker->randomFloat,
        'kitchen_space'        => $faker->randomFloat,
        'living_space'         => $faker->randomFloat,
    ];
});
