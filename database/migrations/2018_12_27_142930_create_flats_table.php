<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flats', function (Blueprint $table) {
            $table->increments('id')->comment('ID квартиры');
            $table->unsignedInteger('bathroom_id')->nullable()->comment('Тип санузла');
            $table->unsignedInteger('repairs_id')->nullable()->comment('Тип ремонта');
            $table->integer('count_rooms')->nullable()->comment('Скольки комнатная квартира');
            $table->integer('floor')->nullable()->comment('Этаж квартиры');
            $table->float('ceiling_height')->nullable()->comment('Высота потолков');
            $table->float('area')->nullable()->comment('Общая площадь');
            $table->float('kitchen_space')->nullable()->comment('Площадь кухни');
            $table->float('living_space')->nullable()->comment('Жилая площадь');
            $table->boolean('studio')->nullable()->comment('Студия');
        });
        Schema::table(
            'flats',
            function (Blueprint $table)
            {
                $table->foreign(
                    'bathroom_id',
                    'flats_bathroom_id_foreign_key')
                    ->references('id')
                    ->on('catalog_bathrooms')
                    ->onUpdate('cascade');

                $table->foreign(
                    'repairs_id',
                    'flats_repairs_id_foreign_key')
                    ->references('id')
                    ->on('catalog_repairs')
                    ->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objects');
    }
}
