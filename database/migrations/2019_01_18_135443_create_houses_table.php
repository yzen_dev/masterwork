<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('houses', function (Blueprint $table) {
            $table->increments('id')->comment('ID Дома');
            $table->float('area')->nullable()->comment('Площадь дома');
            $table->float('lot_area')->nullable()->comment('Площадь участка');
            $table->string('lot_area_unit')->nullable()->comment('Единица измерения');
            $table->float('distance_from_city')->nullable()->comment('Расстояние от города');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('houses');
    }
}
