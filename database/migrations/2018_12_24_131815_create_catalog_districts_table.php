<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogDistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalog_districts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('up_id')->nullable()->comment('Родительский район');
            $table->string('name')->comment('Название района');
            $table->unsignedInteger('city_id')->comment('ID города');

            $table->timestamps();
        });
        Schema::table(
            'catalog_districts',
            function (Blueprint $table)
            {
                $table->foreign(
                    'city_id',
                    'districts_city_id_foreign_key')
                    ->references('id')
                    ->on('catalog_cities')
                    ->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('districts');
    }
}
