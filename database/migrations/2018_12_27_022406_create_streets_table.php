<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStreetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('streets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable()->comment('Название улицы');
            $table->unsignedInteger('city_id')->comment('ID города');
            $table->timestamps();
        });
        Schema::table(
            'streets',
            function (Blueprint $table)
            {
                $table->foreign(
                    'city_id',
                    'districts_city_id_foreign_key')
                    ->references('id')
                    ->on('catalog_cities')
                    ->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('streets');
    }
}
