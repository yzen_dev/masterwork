<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalog_cities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('Название города');
            $table->unsignedInteger('region_id')->comment('ID района/края/области');
            $table->timestamps();
        });
        Schema::table(
            'catalog_cities',
            function (Blueprint $table)
            {
                $table->foreign(
                    'region_id',
                    'cities_region_id_foreign_key')
                    ->references('id')
                    ->on('catalog_regions')
                    ->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
