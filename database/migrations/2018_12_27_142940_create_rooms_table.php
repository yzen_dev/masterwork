<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->increments('id')->comment('ID комнаты');
            $table->integer('floor')->nullable()->comment('Этаж квартиры');
            $table->integer('count_rooms')->nullable()->comment('Количество комнат в квартире');
            $table->float('area')->nullable()->comment('Общая площадь');
            $table->float('room_space')->nullable()->comment('Площадь комнаты');
            $table->float('kitchen_space')->nullable()->comment('Площадь кухни');
            $table->boolean('studio')->nullable()->comment('Студия');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
