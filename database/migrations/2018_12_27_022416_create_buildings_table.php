<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuildingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buildings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('street_id')->nullable()->comment('ID улицы');
            $table->unsignedInteger('building_type_id')->nullable()->comment('Тип дома');
            $table->unsignedInteger('type_housing_id')->nullable()->comment('Тип жилья');
            $table->string('number')->nullable()->comment('Номер дома');
            $table->integer('floors_total')->nullable()->comment('Этажей в доме');
            $table->integer('built_year')->nullable()->comment('Год сдачи (год постройки)');
            $table->timestamps();
        });

        Schema::table(
            'buildings',
            function (Blueprint $table) {
                $table->foreign(
                    'street_id',
                    'buildings_street_id_foreign_key')
                    ->references('id')
                    ->on('streets')
                    ->onUpdate('cascade');
                $table->foreign(
                    'building_type_id',
                    'buildings_building_type_id_foreign_key')
                    ->references('id')
                    ->on('catalog_building_types')
                    ->onUpdate('cascade');

                $table->foreign(
                    'type_housing_id',
                    'buildings_type_housing_id_foreign_key')
                    ->references('id')
                    ->on('catalog_types_housing')
                    ->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buildings');
    }
}
