<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_objects', function (Blueprint $table) {
            $table->increments('id')->comment('ID Объявления');
            $table->unsignedInteger('extended_id')->comment('ID подробной информации');
            $table->string('extended_type')->comment('Класс объявления');
            $table->string('source')->comment('Откуда объявление');
            $table->unsignedInteger('user_id')->comment('ID пользователя');
            $table->unsignedInteger('category_id')->comment('ID категории');
            $table->unsignedInteger('building_id')->nullable()->comment('ID дома');
            $table->unsignedInteger('city_id')->comment('ID города');
            $table->unsignedInteger('district_id')->nullable()->comment('ID района');
            $table->integer('type')->comment('Тип сделки');
            $table->string('full_address')->comment('Полный адрес объекта');
            $table->text('description')->nullable()->comment('Описание объявления');
            $table->json('coordinates')->nullable()->comment('Координаты объекта');
            $table->float('price')->comment('Стоимость объекта');
            $table->json('images')->nullable()->comment('Массив изображений');
            $table->string('term')->comment('Срок аренды');
            $table->timestamps();
        });
        Schema::table(
            'base_objects',
            function (Blueprint $table)
            {
                $table->foreign(
                    'user_id',
                    'base_objects_user_id_foreign_key')
                    ->references('id')
                    ->on('users')
                    ->onUpdate('cascade');

                $table->foreign(
                    'building_id',
                    'base_objects_building_id_foreign_key')
                    ->references('id')
                    ->on('buildings')
                    ->onUpdate('cascade');
                $table->foreign(
                    'category_id',
                    'base_objects_category_id_foreign_key')
                    ->references('id')
                    ->on('catalog_categories')
                    ->onUpdate('cascade');

                $table->foreign(
                    'city_id',
                    'base_objects_city_id_foreign_key')
                    ->references('id')
                    ->on('catalog_cities')
                    ->onUpdate('cascade');

                $table->foreign(
                    'district_id',
                    'base_objects_district_id_foreign_key')
                    ->references('id')
                    ->on('catalog_districts')
                    ->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('base_objects');
    }
}
