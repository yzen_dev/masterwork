<?php

use App\Models\Catalogs\Repair;
use Illuminate\Database\Seeder;

class RepairsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Repair::create([
            'name' => 'Косметический'
        ]);
        Repair::create([
            'name' => 'Евроремонт'
        ]);
        Repair::create([
            'name' => 'Дизайнерский'
        ]);
        Repair::create([
            'name' => 'Без ремонта'
        ]);
    }
}
