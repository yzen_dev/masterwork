<?php

use App\Models\Catalogs\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Квартира',
        ]);
        Category::create([
            'name' => 'Комната',
        ]);
        Category::create([
            'name' => 'Дом',
        ]);
        Category::create([
            'name' => 'Дача',
        ]);
        Category::create([
            'name' => 'Участок',
        ]);
        Category::create([
            'name' => 'Офис',
        ]);
        Category::create([
            'name' => 'Гараж',
        ]);
    }
}
