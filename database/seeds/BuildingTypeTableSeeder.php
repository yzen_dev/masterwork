<?php

use App\Models\Catalogs\BuildingType;
use Illuminate\Database\Seeder;

class BuildingTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BuildingType::create([
            'name' => 'Кирпичный',
        ]);
        BuildingType::create([
            'name' => 'Деревянный',
        ]);
        BuildingType::create([
            'name' => 'Монолитный',
        ]);
        BuildingType::create([
            'name' => 'Панельный',
        ]);
        BuildingType::create([
            'name' => 'Блочный',
        ]);
        BuildingType::create([
            'name' => 'Кирпично-монолитный',
        ]);
        BuildingType::create([
            'name' => 'Сталинский',
        ]);
    }
}
