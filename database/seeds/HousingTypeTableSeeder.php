<?php

use App\Models\Catalogs\HousingType;
use Illuminate\Database\Seeder;

class HousingTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        HousingType::create([
            'name' => 'Вторичка'
        ]);
        HousingType::create([
            'name' => 'Новостройка'
        ]);
    }
}
