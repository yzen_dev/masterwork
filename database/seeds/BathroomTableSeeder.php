<?php

use App\Models\Catalogs\Bathroom;
use Illuminate\Database\Seeder;

class BathroomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bathroom::create([
            'name' => 'Совмещенный',
        ]);
        Bathroom::create([
            'name' => 'Раздельный',
        ]);
    }
}
