<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoryTableSeeder::class);
        $this->call(BathroomTableSeeder::class);
        $this->call(BuildingTypeTableSeeder::class);
        $this->call(HousingTypeTableSeeder::class);
        $this->call(RepairsTableSeeder::class);
        $this->call(RegionCityTableSeeder::class);
    }
}
