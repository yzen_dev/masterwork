<?php

use App\Models\Catalogs\City;
use App\Models\Catalogs\Region;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Seeder;
use PHPHtmlParser\Dom;

class RegionCityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            $dom = new Dom;
            $dom->loadFromUrl('https://33tura.ru/goroda-rossii');
            $rows = $dom->find('tr');
            foreach ($rows as $row) {
                if (count($row->find('td')) === 1) continue;
                $region = Region::firstOrCreate([
                    'name' => $row->find('td')[1]->innerHtml,
                    'code' => $row->find('td')[2]->innerHtml
                ]);
                City::create([
                    'name' => $row->find('td')[0]->innerHtml,
                    'region_id' => $region->id
                ]);
            }
            City::create([
                'name' => 'Не определен'
            ]);

        } catch (GuzzleException $error) {
            dd($error);
        }
    }
}
