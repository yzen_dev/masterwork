let mix = require('laravel-mix');

mix.disableNotifications();
mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css');

mix.copyDirectory('resources/assets/fonts', 'public/fonts');

if (mix.inProduction()) {
    mix.version();
}

mix.browserSync({
    proxy: 'mygaff.develop'
});
