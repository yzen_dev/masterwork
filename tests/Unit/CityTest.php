<?php

namespace Tests\Unit;

use App\Model\Object\ObjectType;
use App\Models\BaseObject;
use App\Models\Catalogs\Bathroom;
use App\Models\Catalogs\City;
use App\Models\Catalogs\Repair;
use App\Models\Flat;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CityTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreated()
    {
        $flat = factory(Flat::class)->create();
        $object = factory(BaseObject::class)->make();
        $object->extended_id = $flat->id;
        $object->extended_type = 'App\Models\Flat';
        $object->create();

        $object2 = City::first()->baseObjects()->first();
        $this->assertEquals($object->id,$object2->id);
        $this->assertEquals($object->city->name,'Чита');
        $this->assertEquals($object->city->region->name,'Забайкальский край');
    }
}
