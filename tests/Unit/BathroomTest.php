<?php

namespace Tests\Unit;

use App\Model\Object\ObjectType;
use App\Models\BaseObject;
use App\Models\Catalogs\Bathroom;
use App\Models\Catalogs\Repair;
use App\Models\Flat;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BathroomTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreated()
    {
        $flat = factory(Flat::class)->create();
        $object = factory(BaseObject::class)->make();
        $object->extended_id = $flat->id;
        $object->extended_type = 'App\Models\Flat';
        $object->create();

        $flat = Bathroom::first()->flats()->first();
        $this->assertEquals($object->id,$flat->baseObject->id);
    }
}
