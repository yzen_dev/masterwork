import Vue from 'vue';
import router from './router';
import store from './store';
import axios from 'axios';
import * as filters from './filters'
import Application from './Application';

Vue.prototype.$axios = axios;

Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key])
});

const app = new Vue({
    el: '#app',
    router: router,
    store: store,
    render: h => h(Application),
});