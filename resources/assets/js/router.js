import Vue from 'vue';
import Router from 'vue-router';
import Home from './pages/Home'
import Report from './pages/Report'

Vue.use(Router);

let router = new Router({
  mode: 'history',
  relative: true,
  routes: [
    {
      path: '/',
      name: 'index',
      component: Home
    },
    {
      path: '/report',
      name: 'report',
      component: Report
    },
  ]
});

export default router;